workflow jgi_bbtools_stats {
    File input_fasta

   call run_stats {
      input: input_fasta=input_fasta
    }
}

task run_stats {
	File input_fasta

    command
    {
        shifter --image=jfroula/jgi-bbstats:1.0.0 bbstats_wrapper.sh ${input_fasta} bbstats.out
	}
    output {
        File out = "bbstats.out"
    }
}
