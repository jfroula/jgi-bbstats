FROM ubuntu:16.04
MAINTAINER jlfroula@lbl.gov

RUN apt-get update && apt-get install -y wget bzip2

# install miniconda
RUN wget https://repo.continuum.io/miniconda/Miniconda2-4.5.11-Linux-x86_64.sh
RUN bash ./Miniconda2-4.5.11-Linux-x86_64.sh -b
RUN rm Miniconda2-4.5.11-Linux-x86_64.sh

# set up environment for next steps
ENV PATH=/root/miniconda2/bin:$PATH

# Create an environmtent for python2 tools
RUN    ["/bin/bash","-c","source activate"]
RUN    conda install -y -c bioconda bbmap=37.75
RUN    ["/bin/bash","-c","source deactivate"]

COPY scripts/* /usr/local/bin

ENTRYPOINT []
