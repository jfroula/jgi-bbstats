# Summary
This tutorial will walk you through the steps to 
1. create a docker image from a Dockerfile
2. register the image at hub.docker.com
3. run the workflow on denovo/cori


# Steps to Run the Example
#### clone this repo
```
git clone git@gitlab.com:jfroula/jgi-bbstats.git
cd jgi-bbstats
```  

#### creating the image
1) create docker image on a machine with docker installed
```
# build in an emtpy directory to keep image size down
mkdir build && cd build
docker build --tag <your_dockerhub_user_name>/jgi-bbstats:1.0.0 ../
```

3) push image to hub.docker.com
```
# login to hub.docker.com 
docker login --username=<your_dockerhub_user_name>
docker push <your_dockerhub_user_name>/jgi-bbstats:1.0.0
```

#### test workflow on final machine (e.g. denovo or cori)
1) on CORI you need to clone the repo again (or copy over first instance of it)
and pull imgage from hub.docker.com
```
git clone git@gitlab.com:jfroula/jgi-bbstats.git
cd jgi-bbstats
shifterimg pull <your_dockerhub_user_name>/jgi-bbstats:1.0.0
```

2) Test the image  
(This line is in the analysis.wdl)
```
shifter --image=<your_dockerhub_user_name>/jgi-bbstats:1.0.0 bbstats_wrapper.sh TestData/PsimiaeWCS417.fasta bbstats.out
```
this creates a bbstats.out file that contains some sequencing stats. There nothing prints to stdout.

#### running workflow
```
java -jar /global/dna/projectdirs/DSI/workflows/cromwell/cromwell.jar run analysis.wdl --inputs inputs.json
```  
If there were no errors, you should see somewhere in the stdout this line:
`[info] SingleWorkflowRunnerActor workflow finished with status 'Succeeded'`

Cromwell puts the output table "bbstats.out" here:
```
cat cromwell-executions/jgi_bbtools_stats/*/call-run_stats/execution/bbstats.out
```
