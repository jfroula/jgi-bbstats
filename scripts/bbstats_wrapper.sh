#!/bin/bash -l
set -euo pipefail

FASTA=$1
TABLE=$2

if [[ ! $FASTA ]] || [[ ! $TABLE ]]; then
    echo "Usage: $0 <fasta> <table>"
    exit 1
fi
if [[ ! -s $FASTA ]]; then 
    echo "Missing or empty file: $FASTA"
    exit 1
fi

bbstats.sh in=$FASTA > $TABLE
